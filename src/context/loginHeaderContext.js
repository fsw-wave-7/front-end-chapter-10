import React from "react";

export const LoginHeaderContext = React.createContext();

export default function LoginHeaderProvider({children}) {
	const [isLogin, setIsLogin] = React.useState(false);
	function getData() {
		setIsLogin(true);
	}

	return <LoginHeaderContext.Provider value={[getData, isLogin]}>{children}</LoginHeaderContext.Provider>;
}
