import React, {useEffect, useState, useContext} from "react";
import {Link} from "react-router-dom";
import {Container, Row, Col, Button} from "react-bootstrap";
import imgrps from "../../../src/assets/images/rps.png";
import rock from "../../../src/assets/images/rock.png";
import paper from "../../../src/assets/images/paper.png";
import scissors from "../../../src/assets/images/scissors.png";
import "../GameRockPaperScissors/GameRockPaperScissors.css";
import {GameContext} from "../../context/gameContext";


const RockPaperScissorsResult = () => {
	let [setMyChoice, myChoice, setScore, score] = useContext(GameContext);
	let [house, setHouse] = useState("");
	let [playerWin, setPlayerWin] = useState("");

	const newHousePick = () => {
		let choices = ["rock", "paper", "scissors"];
		setHouse(choices[Math.floor(Math.random() * 3)]);

		
	};

	useEffect(() => {
		newHousePick();
	}, []);

	const Result = () => {
		if (myChoice === "rock" && house === "scissors") {
			setPlayerWin("win");
			setScore((score += 3));
		} else if (myChoice === "rock" && house === "rock") {
			setPlayerWin("draw");
			setScore(score - 1 + 2);
		} else if (myChoice === "scissors" && house === "paper") {
			setPlayerWin("win");
			setScore((score += 3));
		} else if (myChoice === "scissors" && house === "scissors") {
			setPlayerWin("draw");
			setScore((score += 1));
		} else if (myChoice === "paper" && house === "rock") {
			setPlayerWin("win");
			setScore((score += 3));
		} else if (myChoice === "paper" && house === "paper") {
			setPlayerWin("draw");
			setScore((score += 1));
		} else {
			setPlayerWin("lose");
		}
	};

	useEffect(() => {
		Result();
	}, [house]);

	return (
		<div className="game rps background-game">
			<Container>
				<Row>
					<Col xs={1} className="px-2 py-4 backrps">
						<Link to="/game">
							<Button variant="primary">BACK</Button>
						</Link>
					</Col>
					<Col xs={1} className="px-2 py-4 ml-4">
						<img src={imgrps} fluid />
					</Col>
					<Col xs={4} className="px-2 py-4 judulrps">
						<h2>ROCK PAPER SCISSORS</h2>
					</Col>
				</Row>
				<Row>
					<Col>
						<h3>Score : {score}</h3>
					</Col>
				</Row>
				<Row>
					<Col>
						<div className="py-4">
							My Choice: <br /> <br />
							{myChoice === "rock" && <img src={rock} height="75" />}
							{myChoice === "paper" && <img src={paper} height="75" />}
							{myChoice === "scissors" && <img src={scissors} height="75" />}
						</div>
					</Col>
					<Col>
						<div className="py-4">
							Result: <br /> <br /> <br />
							{playerWin === "win" && <h2>You Win</h2>}
							{playerWin === "lose" && <h2>You Lose</h2>}
							{playerWin === "draw" && <h2>Draw</h2>}
						</div>
					</Col>
					<Col>
						<div className="py-4">
							House Choice: <br /> <br />
							{house === "rock" && <img src={rock} height="75" />}
							{house === "paper" && <img src={paper} height="75" />}
							{house === "scissors" && <img src={scissors} height="75" />}
						</div>
					</Col>
				</Row>

				<Row>
					<Col>
						<div className="py-4">
							<Link to="/game/rock-paper-scissors" onClick={() => setHouse()}>
								<Button variant="primary">PLAY AGAIN</Button>
							</Link>
						</div>
					</Col>
				</Row>
			</Container>
		</div>
	);
};

export default RockPaperScissorsResult;
