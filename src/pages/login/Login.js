import React, { useContext, useRef, useState } from 'react';
import Footer from '../../components/FooterComponent/FooterComponent';
// Contexts
import { AuthContext } from '../../context/authContext';
import { LoginHeaderContext } from '../../context/loginHeaderContext';
// Bootstrap
import { Card, Form, Col, Row, Button } from 'react-bootstrap';
// import CSS
import styles from './login.module.css';
// Google UI
import { GoogleSignIn } from '../../components/FirebaseUI/firebaseUI';
// Route
import { useHistory } from 'react-router-dom';

function Login() {
  // ------CREATE STATE
  const [loadingColor, setLoadingColor] = useState(styles.button);
  const [loadingText, setLoadingText] = useState('Log In');
  const [disabled, setDisabled] = useState('');
  // -------GET CONTEXTS
  const [signUp, login, , user, isLogin] = useContext(AuthContext);
  const [getData, isLoading] = useContext(LoginHeaderContext);
  // -------GET VALUE FROM FORM INPUT
  const emailRef = useRef();
  const passRef = useRef();
  const history = useHistory();

  async function handleSubmit(e) {
    // ------LOADING SETTING
    setLoadingColor(styles.loading);
    setLoadingText('Loading');
    setDisabled('disabled');

    // -----PREVENT FROM REFRESHING
    e.preventDefault();

    // -------LOGIN FUNCTION FROM FIREBASE
    try {
      await login(emailRef.current.value, passRef.current.value);

      // ------FUNCTION FOR LOADING SETTING
      getData();

      console.log(user.email);
    } catch (error) {
      console.log(error);
    }

    // ------REDIRECT IF AUTHENTICATED
    if (emailRef.current.value === user.email) {
      alert('You are authenticated!!');
      history.push('/profile');
    } else {
      setLoadingColor(styles.button);
      setLoadingText('Log In');
      setDisabled('');
      alert('You are NOT authenticated!!');
      history.push('/login');
    }
  }
  return (
    <>
      <div
        className={`${styles.Login} d-flex align-items-center justify-content-center`}
      >
        <Card
          style={{
            maxWidth: '600px',
            minWidth: '400px',
            borderRadius: '5px',
            backgroundColor: '#ffffff2e',
          }}
          className={`d-flex justify-content-center`}
        >
          <Card.Body className="w-100">
            <h1 className="mb-3">Login</h1>
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control ref={emailRef} type="email"></Form.Control>
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control ref={passRef} type="password"></Form.Control>
              </Form.Group>

              <button
                className={`${loadingColor} ${disabled} btn`}
                type="submit"
                value="submit"
              >
                {loadingText}
              </button>
            </Form>
            <div class="container-fluid mt-3">
              <Row>
                <Col>
                  <h4 clasName="">or</h4>
                </Col>
              </Row>
              <Row>
                <Col>
                  <GoogleSignIn />
                </Col>
              </Row>
            </div>
          </Card.Body>
        </Card>
      </div>
      <Footer />
    </>
  );
}

export default Login;
