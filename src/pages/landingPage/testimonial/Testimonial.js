import React, {Component} from "react";
import SectionTitle from "../../../components/TestimonialComponent/SectionTitleComponent";
import TestimonialCard from "../../../components/TestimonialComponent/TestimonialCardComponent";
import {Container} from "react-bootstrap";
import classes from "./Testimonial.module.css";

export default class Testimonial extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			img: "",
		};
	}
	async componentDidMount() {
		const url = "https://api.randomuser.me/";
		const gathering = await fetch(url);
		const data = await gathering.json();
		this.setState({
			name: data.results[0].name.first,
			img: data.results[0].picture.thumbnail,
		});
	}

	render() {
		return (
			<Container fluid className={`${classes.scores}`}>
				<div className={`${classes.vcenter} row d-flex justify-content-center align-items-center`}>
					<div className="col-sm-md-lg-6 m-5 text-left">
						<SectionTitle />
					</div>
					<div className="col-sm-md-lg-6 d-flex flex-column align-self-center">
						<div className="p-2 text-left">
							<TestimonialCard name={this.state.name} pic={this.state.img} job="PC GAMERS" post="One of my gaming highlights of the year." />
						</div>
						<div className="p-2 text-left">
							<TestimonialCard name={this.state.name} pic={this.state.img} job="Nerdreactor" post="The next big thing in the world of streaming and survival games." />
						</div>
						<div className="p-2 text-left">
							<TestimonialCard name={this.state.name} pic={this.state.img} job="Approx" post="Snoop Dogg Playing The Wildly Entertaining ‘SOS’ Is Ridiculous." />
						</div>
					</div>
				</div>
			</Container>
		);
	}
}
