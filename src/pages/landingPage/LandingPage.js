import {React, Component} from "react";

import JumbotronComponent from "../../components/JumbotronComponent/JumbotronComponent";
import GameLists from "./gameLists/GameLists";
import Testimonial from "./testimonial/Testimonial";

import FooterComponent from "../../components/FooterComponent/FooterComponent";

export default class LandingPage extends Component {
	getCurrentUser = () => {};

	render() {
		return (
			<div>
				<JumbotronComponent />
				<GameLists />
				<Testimonial />
				<FooterComponent />
			</div>
		);
	}
}
