import React, {useContext, useEffect, useState} from "react";
import classes from "./GameComponent.module.css";
import {Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
// Contexts
import {AuthContext} from "../../context/authContext";
// Route
import {useHistory} from "react-router-dom";
// API ENDPOINT
import API_ENDPOINT from "../../heroku/api-endpoint";
// AXIOS
import axios from "axios";

const GameListCard = (props) => {
	// -------GET CONTEXTS
	const [signUp, login, , user, isLogin] = useContext(AuthContext);
	// -----SETUP COUNT
	let [data, setData] = useState(0);
	const history = useHistory();

	useEffect(() => {
		(async () => {
			try {
				const response = await axios.get(API_ENDPOINT.GET_ALL_GAMES);
				setData(parseInt(response.data.data[0].play_count));
			} catch (error) {
				console.error(error);
			}
		})();
		// console.log(data);
	}, []);

	async function loginYet(e) {
		e.preventDefault();

		// --------FETCHING API FOR REGISTER
		let addScore = await fetch(API_ENDPOINT.CLICK_COUNT, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				play_count: data + 1,
			}),
		});
		// console.log(data);
		return isLogin ? history.push("/game/rock-paper-scissors") : history.push("/login");
	}
	return (
		<div className={classes.GameListCard}>
			<Card className="align-item-center mt-5" style={{width: "26rem"}} bg="dark">
				<Card.Img className="p-4" variant="top" src={props.img} />
				<Card.Body>
					<Card.Title className="text-white">{props.title}</Card.Title>
					<Card.Text className="text-white">{props.detail}</Card.Text>
					<Link
						to={{
							pathname: props.link,
							state: {
								name: props.title,
								detail: props.detail,
								img: props.img,
								link: props.link,
							},
						}}
					>
						<Button onClick={loginYet} className="px-4 py-2" variant="danger">
							Play Game
						</Button>
					</Link>
				</Card.Body>
			</Card>
		</div>
	);
};

export default GameListCard;
