import React from "react";
import {Row, Col} from "react-bootstrap";
import styles from "./FooterComponent.module.css";
import {Link} from "react-router-dom";

function FooterComponent() {
	return (
		<div className={`${styles.container} container-fluid py-5`}>
			<Row class="justify-content-between py-sm-5 ">
				<Col className="d-flex justify-content-start px-sm-3" sm={6} md={4}>
					<p className="h2" style={{color: "white"}}>
						TEAM <strong style={{fontWeight: "700"}}>FSW 7</strong>
					</p>
				</Col>

				<Col md={4} className="d-flex flex-column align-items-start">
					<h3 className={`${styles.items} mt-sm-2`}>Explore</h3>
					<ul className={`${styles.items} px-0 mt-md-3 d-flex flex-column align-items-start`}>
						<li>
							<Link className={`${styles.items}`} to="/">
								Home
							</Link>
						</li>
						<li>
							<Link className={`${styles.items}`} to="/game">
								Games
							</Link>
						</li>
					</ul>
				</Col>

				<Col md={4} className="d-flex flex-column align-items-start">
					<h3 className={styles.items}>Social Media</h3>
					<ul className={`${styles.iconContainer} mt-3 d-flex justify-content-start px-0`}>
						<li className="pr-3">
							<img src="./assets/icons/fb.png" width="50px" />
						</li>
						<li className="pr-3">
							<img src="./assets/icons/twit.png" width="50px" />
						</li>
						<li className="pr-3">
							<img src="./assets/icons/google.png" width="50px" />
						</li>
						<li className="pr-3">
							<img src="./assets/icons/ig.png" width="50px" />
						</li>
						<li className="pr-3">
							<img src="./assets/icons/in.png" width="50px" />
						</li>
						<li className="pr-3">
							<img src="./assets/icons/github.png" width="50px" />
						</li>
					</ul>
				</Col>
			</Row>
		</div>
	);
}

export default FooterComponent;
